import requests
from bs4 import BeautifulSoup
import pandas as pd


def load_file_to_list(filename: str) -> list:
    result = []
    with open(filename, encoding='utf-8') as fh:
        for line in fh:
            cleaned_line = line.replace("\n", "")
            result.append(cleaned_line)
    return result


def get_opinion_score_df_from_single_url(url: str) -> pd.DataFrame:

    header = {'User-Agent': 'Mozilla/5.0'}

    # predefined classes for opinion and score
    opinions_class = "user-post__text"
    scores_class = "user-post__score-count"

    # getting response
    try:
        response = requests.get(url, headers=header)
        response.encoding = 'utf-8'
    except ConnectionError:
        # returns empty df when url doesn't exist
        return pd.DataFrame()

    # creating soup object
    soup = BeautifulSoup(response.text, 'lxml')

    opinions = soup.find_all(class_=opinions_class)
    scores = soup.find_all(class_=scores_class)

    # parsing soup elements to list
    opinion_list = [opinion.get_text() for opinion in opinions[0::2]]
    score_list = [score.get_text() for score in scores]
    score_list_fixed = [score[:score.index("/")] for score in score_list]

    return pd.DataFrame({"Opinion": opinion_list, "Score": score_list_fixed})


def get_opinion_score_df_from_multiple_urls(urls: list) -> pd.DataFrame:
    dfs = []
    for url in urls:
        try:
            dfs.append(get_opinion_score_df_from_single_url(url))
        except Exception as e:
            print(e)

    return pd.concat(dfs)


if __name__ == "__main__":
    url_list = load_file_to_list("urls.txt")
    print(len(url_list))

    df = get_opinion_score_df_from_multiple_urls(url_list)

    df.to_csv("data.csv", index=False)
